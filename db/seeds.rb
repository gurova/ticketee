admin = User.create!(name: "Alexandra Gurova",
                     email: "alexandragurova@yandex.ru",
                     password: "password",
                     admin: true)
             
calvin = User.create!(name: "Calvin",
                       email: "calvin@example.com",
                       password: "foobar") 
             
hobbes = User.create!(name: "Hobbes",
                       email: "hobbes@example.com",
                       password: "foobar")
             
firefox = Project.create!(name: "Mozilla Firefox",
                          description: "Cross-platfom browser")

ie = Project.create!(name: "Internet Explorer",
                    description: "Ohh jess...")
                    
ticket1 = Ticket.create!(title: "More colors",
                        description: "Make it shiny!",
                        project: firefox,
                        user: admin)
ticket2 = Ticket.create!(title: "Firefox for android",
                        description: "Need more speed",
                        project: firefox,
                        user: calvin)
					 

50.times do |n|
	name = Faker::Name.name
	email = "example-#{n+1}@example.org"
	password = "foobar"
	User.create!(name: name,
	email: email,
	password: password)
end

users = User.order(:created_at).take(6)

50.times do
	project_name = Faker::Commerce.product_name
	project_description = Faker::Lorem.sentence(20)
	Project.create!(name: project_name, 
					        description: project_description) 
end

projects = Project.order(:created_at).take(20)

users.each do |user|
  10.times do
  	ticket_title = Faker::Company.catch_phrase
  	ticket_description = Faker::Lorem.sentence(12)
  	projects.each do |project|
  		project.tickets.create!(title: ticket_title,
            									description: ticket_description,
            									user: user)
  	end
  end
end
                
State.create!(name: "New",
              background: "#85FF00",
              color: "white",
              default: true)
                
State.create!(name: "Open",
              background: "#00CFFD",
              color: "white")
                
State.create!(name: "Closed",
              background: "black",
              color: "white")