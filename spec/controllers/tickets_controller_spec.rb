require 'rails_helper'

RSpec.describe TicketsController, :type => :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project) }
  let(:ticket) { FactoryGirl.create(:ticket, project: project, user: user) }
  
  context "standard users" do
    it "can not access a ticket for a project" do
      sign_in(user)
      get :show, :id => ticket.id, :project_id => project.id
      
      expect(response).to redirect_to(root_path)
      expect(flash[:alert]).to have_content("The project you were looking for " +
                                            "could not be found.")
    end
    
    context "with permission to view the project" do
      before do
        sign_in(user)
        define_permission!(user, "view", project)
      end
      
      def cannot_create_tickets!
        expect(response).to redirect_to(project)
        message = "You cannot create tickets on this project."
        expect(flash[:alert]).to eql(message)
      end
      
      def cannot_update_tickets!
        expect(response).to redirect_to(project)
        message = "You cannot edit tickets on this project."
        expect(flash[:alert]).to eql(message)
      end
      
      def cannot_delete_tickets!
        expect(response).to redirect_to(project)
        message = "You cannot delete tickets from this project."
        expect(flash[:alert]).to eql(message)
      end   
      
      it "cannot begin to create a ticket" do
        get :new, project_id: project.id
        cannot_create_tickets!
      end
      
      it "cannot create a ticket without permission" do
        post :create, project_id: project.id
        cannot_create_tickets!
      end
      
      it "cannot edit a ticket without permission" do
        get :edit, { project_id: project.id, id: ticket.id }
        cannot_update_tickets!
      end
      
      it "cannot update a ticket without permission" do
        put :update, { project_id: project.id, 
                        id: ticket.id,
                        ticket: {} }
        cannot_update_tickets!
      end
      
      it "cannot delete a ticket without permission" do
        delete :destroy, { project_id: project.id, id: ticket.id }
        cannot_delete_tickets!
      end
      
      it "can create tickets, but not tag them" do
        define_permission!(user, "create tickets", project)
        post :create, ticket: { project_id: project.id, 
                                ticket: { title: "New Ticket",
                                          description: "Brand spankin' new",
                                          tag_names: "these are tags"
                                        }
                              }
        expect(ticket.tags).to be_empty
      end
    end
  end
end
