require 'rails_helper'

RSpec.describe CommentsController, :type => :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project) }
  let(:ticket) { FactoryGirl.create(:ticket, project: project, user: user) }
  
  context "a user without permission to tag a ticket" do
    before do
      sign_in(user)
    end
    
    it "cannot tag a ticket when creating a comment" do
      post :create, { ticket_id: ticket.id, comment: { text: "Text", tag_names: "one two" } }
      ticket.reload
      expect(ticket.tags).to be_empty
    end
  end
end
