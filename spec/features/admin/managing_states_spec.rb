require "rails_helper"

feature 'Managing States' do
  let!(:state) { FactoryGirl.create(:state, name: "New") }
  
  before do
    sign_in_as!(FactoryGirl.create(:admin_user))
  end
  
  scenario "Marking a state as default" do
    visit '/'
    click_link 'Admin'
    click_link 'States'
    within state_line_for(state.name) do
      click_link "Make Default"
    end
    
    expect(page).to have_content("New is now the default state.")
  end
end