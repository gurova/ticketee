require "rails_helper"

feature 'Creating comments' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:project) { FactoryGirl.create(:project) }
  let!(:ticket) { FactoryGirl.create(:ticket, project: project, user: user) }
  let!(:state) { FactoryGirl.create(:state, name: "Open") }
  
  before do
    define_permission!(user, "view", project)
    define_permission!(user, "change states", project)
    define_permission!(user, "tag", project)
    
    sign_in_as!(user)
    visit '/'
    click_link project.name
    click_link ticket.title
  end
  
  scenario "Creating a comment" do
    fill_in "Text", with: "Added a comment!"
    click_button "Create Comment"
    expect(page).to have_content("Comment has been created.")
    within("#comments") do
      expect(page).to have_content("Added a comment!")
    end
  end
  
  scenario "Creating an invalid comment" do
    click_button "Create Comment"
    expect(page).to have_content("Comment has not been created.")
    expect(page).to have_content("Text can't be blank")
  end
  
  scenario "Changing a ticket's state" do
    fill_in "Text", with: "This is a real issue"
    select "Open", from: "State"
    click_button "Create Comment"
    expect(page).to have_content("Comment has been created.")
    within("#ticket .state") do
      expect(page).to have_content("Open")
    end
    within("#comments") do
      expect(page).to have_content("State: Open")
    end
  end
  
  scenario "Adding a tag to a ticket" do
    within("#ticket #tags") do
      expect(page).to_not have_content("bug")
    end
    fill_in "Text", with: "Adding the bug tag"
    fill_in "Tags", with: "bug"
    
    click_button "Create Comment"
    expect(page).to have_content("Comment has been created.")
    within("#ticket #tags") do
      expect(page).to have_content("bug")
    end
  end
end