require "rails_helper"

feature 'Profile page' do
  scenario "Viewing profile page" do
    user = FactoryGirl.create(:user)
    
    visit user_path(user)
    expect(page).to have_content(user.name)
    expect(page).to have_content(user.email)
  end
end

feature 'Updating Users' do
  scenario "Updating user profile" do
    user = FactoryGirl.create(:user)
    
    visit user_path(user)
    click_link "Edit Profile"
    
    fill_in "Username", with: "new_username"
    click_button "Update Profile"
    
    expect(page).to have_content("Profile has been updated.")
  end
end