require "rails_helper"

RSpec.describe Notifier, :type => :mailer do
  context "comment updated" do
    let!(:project) { FactoryGirl.create(:project) }
    let!(:creator) { FactoryGirl.create(:user) }
    let!(:ticket) { FactoryGirl.create(:ticket, project: project, user: creator) }
    let!(:commenter) { FactoryGirl.create(:user) }
    let!(:comment) do 
      Comment.new( ticket: ticket, user: commenter, text: "Comment1")
    end
    let(:email) do
      Notifier.comment_updated(comment, creator)
    end
    
    it "sends out an email notification about a new comment" do
      expect(email.to).to include(creator.email)
      title = "#{ticket.title} for #{project.name} has been updated."
      expect(email.body).to include(title)
      expect(email.body).to include("#{comment.user.email} wrote:")
      expect(email.body).to include(comment.text)
    end
  end
end
