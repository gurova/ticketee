require 'rails_helper'

RSpec.describe User, :type => :model do
  describe "password" do
    it "needs a password and confirmation to save" do
      u = User.new(name: "alexandra", email: "alexandra@example.org")
      
      u.save
      expect(u).to_not be_valid
      
      u.password = "password"
      u.password_confirmation = ""
      u.save
      expect(u).to_not be_valid
      
      u.password_confirmation = "password"
      u.save
      expect(u).to be_valid
    end
    
    it "needs a password and confirmation to match" do
      u = User.create(name: "alexandra",
                      password: "foobar",
                      password_confirmation: "foobar2")
      expect(u).to_not be_valid
    end
  end
  
  describe "authentication" do
    let(:user) { User.create(name: "alexandra",
                              password: "foobar",
                              password_confirmation: "foobar") }
                              
    it "authentication with a correct password" do
      expect(user.authenticate("foobar")).to be
    end
    
    it "does not authenticate with an incorrect password" do
      expect(user.authenticate("foobar3")).to_not be
    end
  end
  
  describe "email" do
    it "requires an email" do
      u = User.new(name: "jim",
                   password: "foobar",
                   password_confirmation: "foobar")
      u.save
      expect(u).to_not be_valid
      
      u.email = "jim@example.org"
      u.save
      expect(u).to be_valid
    end
  end
end
