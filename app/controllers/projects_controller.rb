class ProjectsController < ApplicationController
  before_action :authorize_admin!, except: [:index, :show]
  before_action :require_signin!, only: [:show, :index]
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  
  def index
    @projects = Project.for(current_user).page(params[:page])
    # @projects = Project.all
  end
  
  #controller actions do not need to exist in the controller
  #if there are templates corresponding to those actions
  def show
    @tickets = @project.tickets.page(params[:page])
  end 
  
  def new
    @project = Project.new  
  end
  
  def create
    @project = Project.new(project_params)
    
    if @project.save
      redirect_to @project, notice: "Project has been created."
    else
      flash[:alert] = "Project has not been created."
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @project.update(project_params)
      redirect_to @project, notice: "Project has been updated."
    else
      flash[:alert] = "Project has not been updated."
      render 'edit'
    end
  end
  
  def destroy
    @project.destroy
    
    redirect_to projects_path, notice: "Project has been deleted."
  end
  
  private
    
    def project_params
      params.require(:project).permit(:name, :description)
    end
    
    def set_project
      @project = Project.for(current_user).find(params[:id])
      
      # if current_user.admin?
      #   @project = Project.find(params[:id])
      # else
      #   @project = Project.viewable_by(current_user).find(params[:id])
      # end
    rescue ActiveRecord::RecordNotFound
      flash[:alert] = "The project you were looking for could not be found."
      redirect_to projects_path
    end
end
