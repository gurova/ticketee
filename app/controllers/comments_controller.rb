class CommentsController < ApplicationController
  before_action :require_signin!
  before_action :set_ticket
  before_action :authorize_change_state!
  before_action :authorize_tagging!
  
  def create
    @comment = @ticket.comments.build(comment_params)
    @comment.user = current_user
    
    if @comment.save
      flash[:notice] = "Comment has been created."
      redirect_to [@ticket.project, @ticket]
    else
      @states = State.all
      flash[:alert] = "Comment has not been created."
      render 'tickets/show'
    end
  end
  
  private
  
    def comment_params
      params.require(:comment).permit(:text, :state_id, :tag_names)
    end
    
    def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end
    
    def authorize_change_state!
      if !current_user.admin? && cannot?("change states".to_sym, @ticket.project)
        flash[:alert] = "You cannot change ticket's state."
        params[:comment].delete(:state_id)
      end
    end
    
    def authorize_tagging!
      if !current_user.admin? && cannot?("tag".to_sym, @ticket.project)
        flash[:alert] = "You cannot add tags."
        params[:comment].delete(:tag_names)
      end
    end
end
